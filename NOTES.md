# Uso de @pmoo/testy en Proyectos Typescript
## Instalación de dependencias
```bash
npm install --save-dev typescript ts-node @types/node
npm install --save-dev @pmoo/testy
npm install --save-dev eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin
tsc --init
```

Si no se usa `eslint` se puede no ignrar la línea

```bash
npm install --save-dev eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin
```

### Types de @pmoo/testy para Typescript

1. Agregar el typing en `tsconfig.json`
```json
    "baseUrl": "./",
    "paths": {
      "@pmoo/testy": ["typings/@pmoo/testy"]
    },
```

2. Crear el un archivo `typings/@pmoo/testy/index.d.ts` con el siguiente contenido:

```
declare module '@pmoo/testy';
```
## Ejectuar tests

Para ejecutar tests de código `Typescript` mediante `testy` hay que utilizar `ts-node`.

`ts-node ts-node ./node_modules/.bin/testy`

### Debuggear tests en VSCode con Testy

En el archivo `launch.json` agregar la siguiente configuración:

```json
  {
    "name": "Launch TypeScript",
    "type": "node",
    "request": "launch",
    "runtimeArgs": ["--nolazy", "-r", "ts-node/register"],
    "args": ["./node_modules/.bin/testy"],
    "cwd": "${workspaceFolder}",
    "internalConsoleOptions": "openOnSessionStart",
    "skipFiles": ["<node_internals>/**", "node_modules/**"],
    "env": {
      "TS_NODE_PROJECT": "${workspaceFolder}/tsconfig.json"
    }
  }
```