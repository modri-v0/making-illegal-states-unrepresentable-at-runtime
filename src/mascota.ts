import { Deserializable, SerializationHelper } from "./serialization";

/**
 * Si bien es posible declarar metodos
 * interfaces usualmente en Typescript
 * intentan expresar estructura
 * Este ejemplo va en ese sentido
 * 
 * Efecto colateral de estas interfaces
 * es que todas las propiedades son publicas
 */
export interface InterfazDeAnimal {
    nombre: string;
}

export interface InterfazDePersona {
    nombre: string,
    profesion: string
}

export class Persona implements InterfazDePersona, Deserializable<Persona> {
    static EL_NOMBRE_NO_PUEDE_ESTAR_EN_BLANCO = 'El nombre no puede estar en blanco';
    static LA_PROFESION_NO_PUEDE_ESTAR_EN_BLANCO = 'La profesion no puede estar en blanco';

    nombre: string;
    profesion: string;
    types: { [index: string]: any; };

    public static DeNombre(unNombre: string, unaProfesion: string): Persona {
        if(unNombre.trim().length === 0) throw new Error(Persona.EL_NOMBRE_NO_PUEDE_ESTAR_EN_BLANCO);
        if(unaProfesion.trim().length === 0) throw new Error(Persona.LA_PROFESION_NO_PUEDE_ESTAR_EN_BLANCO);

        return new Persona(unNombre, unaProfesion);
    }

    constructor(unNombre: string, unaProfesion: string) {
        this.types = { 
            'nombre': String,
            'profesion': String
        };
        this.nombre =unNombre;
        this.profesion = unaProfesion;
    }


    deserealize(object: any): Persona {
        this.nombre = SerializationHelper.deserializarUsandoMetodoClase(String, object.nombre) as string;
        this.profesion = SerializationHelper.deserializarUsandoMetodoClase(String, object.profesion) as string;
        return this;
    }

    public seDedicaA(posibleProfesion: string): boolean {
        return this.profesion === posibleProfesion;
    }
    public seLlama(posibleNombre: string): boolean {
        return this.nombre === posibleNombre;
    }

    public equals(otraPersona: Persona): boolean {
        return this.nombre === otraPersona.nombre &&
               this.profesion === otraPersona.profesion;
    }
}

export class Animal implements InterfazDeAnimal {
    static EL_NOMBRE_NO_PUEDE_ESTAR_EN_BLANCO = 'El nombre no puede estar en blanco';

    public readonly nombre: string;

    public static DeNombre(unNombre: string):Animal {
        if(unNombre.trim().length === 0) throw new Error(Animal.EL_NOMBRE_NO_PUEDE_ESTAR_EN_BLANCO);
        return new Animal(unNombre);
    } 

    constructor(unNombre: string) {
        this.nombre = unNombre;
    }

    seLlama(posibleNombre: string) : boolean{
        return this.nombre === posibleNombre;
    }
}

export class Mascota extends Animal implements Deserializable<Mascota> {
    private duenio: Persona;
    types: { [index: string]: any; };

    static De(unDuenio: Persona, unNombre: string): Mascota {
        if(unNombre.trim().length === 0) throw new Error(Animal.EL_NOMBRE_NO_PUEDE_ESTAR_EN_BLANCO);
        return new Mascota(unDuenio, unNombre);
    }

    constructor(unDuenio: Persona, unNombre: string) {
        super(unNombre);
        this.duenio = unDuenio;
        this.types = { 
            'nombre': String,
            'duenio': Persona
        };
    }
 
    deserealize(object: any): Mascota {
        const nombre = SerializationHelper.deserializarUsandoMetodoClase(String, object.nombre) as string;
        const duenio = SerializationHelper.deserializarUsandoMetodoClase(Persona, object.duenio);
        return Mascota.De(duenio, nombre);
    }

    esDuenio(posibleDuenio: Persona): boolean {
        return this.duenio.equals(posibleDuenio);
    }

}

/**
 * Chequea si una variable es del tipo InterfazDeAnimal usando la existencia de la propiedad _nombre
 * @param {unknown} posibleAnimal variable a la cual se quiere verificar si es del tipo InterfazDeAnimal
 * @returns {boolean} Verdadero (true) si la variable es un objeto no nulo con la propiedad _nombre. Falso (false) en otro caso. 
 */
export function EsUnaInterfazDeAnimal__UsandoPropiedad(posibleAnimal: unknown): posibleAnimal is InterfazDeAnimal {
    return  typeof posibleAnimal === "object" && posibleAnimal != null &&
     'nombre' in posibleAnimal;
}

/**
 * Chequea si una variable es una instancia de Animal usando la existencia de la propiedad _nombre
 * @param {unknown} posibleAnimal variable a la cual se quiere verificar si es una instancia de Animal
 * @returns {boolean} Verdadero (true) si la variable es un objeto no nulo con la propiedad _nombre. Falso (false) en otro caso. 
 */
export function EsUnAnimal__UsandoPropiedad(posibleAnimal: unknown): posibleAnimal is Animal {
    return  typeof posibleAnimal === "object" && posibleAnimal != null &&
     'nombre' in posibleAnimal;
}

/**
 * Chequea si una variable es una instancia de Animal verificando si es una instancia de la clase 
 * @param {unknown} possibleAnimal variable a la cual se quiere verificar si es una instancia de Animal
 * @returns {boolean} Verdadero (true) si es una instancia de la clase Animal. Falso (false) en otro caso. 
 */
export function EsUnAnimal__UsandoTipoDeInstancia(possibleAnimal: unknown): possibleAnimal is Animal  {
    return typeof possibleAnimal === "object" && possibleAnimal != null && possibleAnimal instanceof Animal;
}

