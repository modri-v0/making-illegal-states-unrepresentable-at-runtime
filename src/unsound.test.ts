import {suite, test, assert} from '@pmoo/testy';
import { EsUnAnimal__UsandoPropiedad, InterfazDePersona } from './mascota';

/**
 * Test para mostrar porque se dice que Typescript es unsound como sistema de tipos
 * Referencia: https://www.typescriptlang.org/docs/handbook/type-compatibility.html#a-note-on-soundness
 */
suite('Typescript Unsoundness', () => {
  
    test('Un objeto es de un tipo aunque tenga propiedades de más', () => {

      interface Perro {
          nombre: string
      }
  
      const marley: InterfazDePersona = { nombre: 'Marley', profesion: 'presentador'};
      const marleyTheDog: Perro = marley;
      assert.that(marleyTheDog.nombre).isEqualTo('Marley');
      //Una instancia de tipo
      //Las propiedades siguen ahí pero no se pueden acceder
      assert.that('profesion' in marleyTheDog).isEqualTo(true);
      //Esta linea es invalidada por el linter
      //assert.that(marleyTheDog['profesion']).isEqualTo('presentador');
  });

  test('Si una instancia de un tipo se identifica con otro tipo usando un type guard se pueden seguir accediendo a sus propiedades', () => {
    const marley: InterfazDePersona = { nombre: 'Marley', profesion: 'presentador' };
    if (EsUnAnimal__UsandoPropiedad(marley)) {
        //A partir de aqui possibleAnimal es instancia de Animal
        assert.that(marley.nombre).isEqualTo('Marley');
        assert.that(() => marley.seLlama('Marley')).raises(TypeError('marley.seLlama is not a function'));
        //Las propiedades siguen ahí y en este caso, por el ambito de ejecucion (scope)
        // es posible accederlas, entonces:
        // ¿Marley es una persona o un Animal?
        assert.that('profesion' in marley).isEqualTo(true);
        assert.that(marley['profesion']).isEqualTo('presentador');
    }
});

});


