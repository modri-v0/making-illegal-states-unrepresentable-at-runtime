import { fail, suite, test, assert } from '@pmoo/testy';
import { Mascota, Persona } from './mascota';
import { SerializationHelper } from './serialization';

/**
 * Tests para mostrar problemas en la deserializacion de JSON a instancias de clases
 */

suite('Serialization', () => {
    // TODO Armamos estos tests y estamos.
    test('?', () => {
        assert.that(true).isEqualTo(true);
    });

});


suite('Deserializacion con propiedad types (JSON a instancia de clase)', () => {

    // false / null / true / object / array / number / string

    test('deserializar true value', () => {
        const deserealized = SerializationHelper.deserializarUsandoTypesDict(Boolean, true);
        assert.that(deserealized).isEqualTo(true);
    } );

    test('deserializar false value', () => {
        const deserealized = SerializationHelper.deserializarUsandoTypesDict(Boolean, false);
        assert.that(deserealized).isEqualTo(false);
    } );

    test('deserializar number value', () => {
        const deserealized = SerializationHelper.deserializarUsandoTypesDict(Number, 0);
        assert.that(deserealized).isEqualTo(0);
    } );

    test('deserializar string value', () => {
        const anString = 'an String';
        const deserealized = SerializationHelper.deserializarUsandoTypesDict(String, anString);
        assert.that(deserealized).isEqualTo(anString);
    } );

    test('deserealize Persona instance', () => {
        const personaObject = {
            nombre: 'Dorothy',
            profesion: 'granjera'
        };

        const deserealized = SerializationHelper.deserializarUsandoTypesDict(Persona, personaObject);
        if(deserealized instanceof Persona) {
            assert.that(deserealized.seLlama(personaObject.nombre)).isEqualTo(true); //, 'deserealized no tiene el mismo nombre que personaObject');
            assert.that(deserealized.seDedicaA(personaObject.profesion)).isEqualTo(true);//, 'deserealized no se dedica a lo mismo que personaObject');
        } else {
            fail.with('deserealized no es instancia de la clase Persona');
        }
    });

    test('deserealize Mascota instance', () => {
        const personaObject = {
            nombre: 'Dorothy',
            profesion: 'granjera'
        };

        const expectedPersona = SerializationHelper.deserializarUsandoTypesDict(Persona, personaObject);

        const mascotaObject = {
            nombre: 'Toto',
            duenio: personaObject
        }

        const deserealized = SerializationHelper.deserializarUsandoTypesDict(Mascota, mascotaObject);
        if(deserealized instanceof Mascota) {
            assert.that(deserealized.seLlama(mascotaObject.nombre)).isEqualTo(true);
            assert.that(deserealized.esDuenio(expectedPersona)).isEqualTo(true);
        } else {
            fail.with('deserealized no es instancia de la clase Mascota');
        }
    });

    test('deserealize otro objeto como Mascota instance', () => {
        const personaObject = {
            nombre: 'Dorothy',
        };

        const expectedPersona = SerializationHelper.deserializarUsandoTypesDict(Persona, personaObject);

        const otroObject = {
            nombre: 'Toto',
            anio: '1978',
            banda: 'Toto',
            tipo: 'Album',
            duenio: personaObject
        }

        const deserealized = SerializationHelper.deserializarUsandoTypesDict(Mascota, otroObject);
        if(deserealized instanceof Mascota) {
            assert.that(deserealized.seLlama(otroObject.nombre)).isEqualTo(true);
            assert.that(deserealized.esDuenio(expectedPersona)).isEqualTo(true);
        } else {
            fail.with('deserealized no es instancia de la clase Mascota');
        }
    });

    test('deserealize objeto invalido como Mascota instance', () => {
        const personaObject = {
            name: 'Dorothy',
        };

        const otroObject = {
            name: 'Toto',
            year: '1978',
            band: 'Toto',
            kind: 'Album',
            owner: personaObject
        }
        
        assert.that(() => SerializationHelper.deserializarUsandoTypesDict(Mascota, otroObject)).raises(TypeError(`Reflect.get called on non-object`));
    });
    
});

suite('Deserializacion con metodo deserealize (JSON a instancia de clase)', () => {

    // false / null / true / object / array / number / string

    test('deserializar true value', () => {
        const deserealized = SerializationHelper.deserializarUsandoMetodoClase(Boolean, true);
        assert.that(deserealized).isEqualTo(true);
    } );

    test('deserializar false value', () => {
        const deserealized = SerializationHelper.deserializarUsandoMetodoClase(Boolean, false);
        assert.that(deserealized).isEqualTo(false);
    } );

    test('deserializar number value', () => {
        const deserealized = SerializationHelper.deserializarUsandoMetodoClase(Number, 0);
        assert.that(deserealized).isEqualTo(0);
    } );

    test('deserializar string value', () => {
        const anString = 'an String';
        const deserealized = SerializationHelper.deserializarUsandoMetodoClase(String, anString);
        assert.that(deserealized).isEqualTo(anString);
    } );

    test('deserealize Persona instance', () => {
        const personaObject = {
            nombre: 'Dorothy',
            profesion: 'granjera'
        };

        const deserealized = SerializationHelper.deserializarUsandoMetodoClase(Persona, personaObject);
        if(deserealized instanceof Persona) {
            assert.that(deserealized.seLlama(personaObject.nombre)).isEqualTo(true); //, 'deserealized no tiene el mismo nombre que personaObject');
            assert.that(deserealized.seDedicaA(personaObject.profesion)).isEqualTo(true);//, 'deserealized no se dedica a lo mismo que personaObject');
        } else {
            fail.with('deserealized no es instancia de la clase Persona');
        }
    });

    test('deserealize Mascota instance', () => {
        const personaObject = {
            nombre: 'Dorothy',
            profesion: 'granjera'
        };

        const expectedPersona = SerializationHelper.deserializarUsandoMetodoClase(Persona, personaObject);

        const mascotaObject = {
            nombre: 'Toto',
            duenio: personaObject
        }

        const deserealized = SerializationHelper.deserializarUsandoMetodoClase(Mascota, mascotaObject);
        if(deserealized instanceof Mascota) {
            assert.that(deserealized.seLlama(mascotaObject.nombre)).isEqualTo(true);
            assert.that(deserealized.esDuenio(expectedPersona)).isEqualTo(true);
        } else {
            fail.with('deserealized no es instancia de la clase Mascota');
        }
    });

    test('deserealize otro tipo de objeto como Mascota instance', () => {
        const personaObject = {
            nombre: 'Dorothy',
        };


        const expectedPersona = SerializationHelper.deserializarUsandoMetodoClase(Persona, personaObject);


        const otroObject = {
            nombre: 'Toto',
            anio: '1978',
            banda: 'Toto',
            tipo: 'Album',
            duenio: personaObject
        }

        const deserealized = SerializationHelper.deserializarUsandoMetodoClase(Mascota, otroObject);
        if(deserealized instanceof Mascota) {
            assert.that(deserealized.seLlama(otroObject.nombre)).isEqualTo(true);
            assert.that(deserealized.esDuenio(expectedPersona)).isEqualTo(true);
        } else {
            fail.with('deserealized no es instancia de la clase Mascota');
        }
    });

    test('deserealize objeto invalido como Mascota instance', () => {
        const personaObject = {
            name: 'Dorothy',
        };

        const otroObject = {
            name: 'Toto',
            year: '1978',
            band: 'Toto',
            kind: 'Album',
            owner: personaObject
        }
        assert.that(() => SerializationHelper.deserializarUsandoMetodoClase(Mascota, otroObject)).raises(TypeError(`Cannot read property 'nombre' of undefined`));

    });

});
