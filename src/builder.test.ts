import { Animal } from "./mascota";
import { suite, test, assert } from '@pmoo/testy';
import { Builder as BuilderV124 } from "./builder-v124";
import { Builder as BuilderV130 } from "./builder-v130";

/**
 * Suite para mostrar potenciales problemas en soluciones
 * como las de Builder-Pattern v1.24
 * https://github.com/Vincent-Pang/builder-pattern/tree/v1.2.4
 */
suite('Uso de Builder-Pattern https://github.com/Vincent-Pang/builder-pattern', () => {
    test('v1.24 crea una instancia de clase invalida al llamar a build', () => {
        const possibleAnimal: Animal = BuilderV124(Animal, {}).build();
        assert.that(possibleAnimal.seLlama('Toto')).isEqualTo(false);
        assert.that(possibleAnimal.nombre).isUndefined();
    });

    test('v1.24 Mal usada tambien crea una instancia de clase invalida al llamar a build', () => {
        //Mal usada la biblioteca <Classe> es segun los docs para Interfaces
        const posibleAnimal: Animal = BuilderV124<Animal>().build();
        assert.that(posibleAnimal.nombre).isUndefined();
        assert.that(() => posibleAnimal.seLlama('Toto')).raises(TypeError('posibleAnimal.seLlama is not a function'));
    });

    test('v1.30 crea una instancia de clase invalida al llamar a build', () => {
        class InfoDeUsuario {
            id!: number;
            nombreUsuario!: string;
            email!: string;

            seLlama(posibleNombre: string) : boolean{
                return this.nombreUsuario === posibleNombre;
            }

        }
        const possibleUsuario: InfoDeUsuario = BuilderV130(InfoDeUsuario).build();
        assert.that(possibleUsuario.seLlama('Pedro')).isEqualTo(false);
        assert.that(possibleUsuario.id).isUndefined();
        assert.that(possibleUsuario.nombreUsuario).isUndefined();
        assert.that(possibleUsuario.email).isUndefined();
    });
});
