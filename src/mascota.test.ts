import { Animal, EsUnaInterfazDeAnimal__UsandoPropiedad, EsUnAnimal__UsandoPropiedad, EsUnAnimal__UsandoTipoDeInstancia, InterfazDeAnimal, Mascota, Persona } from "./mascota";
import { suite, test, assert } from '@pmoo/testy';
import { SerializationHelper } from "./serialization";

const TOTO_NOMBRE = 'Toto';
const DOROTHY_NOMBRE = 'Dorothy';
const DOROTHY_PROFESION = 'granjera';
suite('Animal', () => {

    test('no se puede crear sin un nombre', () => {
        assert.that(() => Animal.DeNombre('')).raises(Error(Animal.EL_NOMBRE_NO_PUEDE_ESTAR_EN_BLANCO));
    });

    test('se puede crear con un nombre que no sea en blanco', () => {
        const toto: Animal = Animal.DeNombre(TOTO_NOMBRE);
        assert.that(toto.seLlama(TOTO_NOMBRE)).isEqualTo(true);
    });

    test('seLlama retorna falso si se llama con otro nombre', () => {
        const toto: Animal = Animal.DeNombre(TOTO_NOMBRE);
        assert.that(toto.seLlama('Marley')).isEqualTo(false);
    });

});

suite('EsUnaInterfazDeAnimal__UsandoPropiedad', () => {
    test('si es del tipo InterfazDeAnimal retorna true', () => {
        const toto: InterfazDeAnimal = {
            nombre: TOTO_NOMBRE
        };

        assert.that(EsUnaInterfazDeAnimal__UsandoPropiedad(toto)).isEqualTo(true);
    });

    test('si no es del tipo InterfazDeAnimal retorna false', () => {
        const falsoToto: unknown = {};
        assert.that(EsUnaInterfazDeAnimal__UsandoPropiedad(falsoToto)).isEqualTo(false);
    });

});

suite('EsUnAnimal__UsandoPropiedad', () => {
    test('si es del tipo Animal retorna true', () => {
        const toto: InterfazDeAnimal = {
            nombre: TOTO_NOMBRE
        };

        assert.that(EsUnAnimal__UsandoPropiedad(toto)).isEqualTo(true);
    });

    test('si no es del tipo Animal retorna false', () => {
        const falsoToto: unknown = {};
        assert.that(EsUnAnimal__UsandoPropiedad(falsoToto)).isEqualTo(false);
    });

});
suite('EsUnAnimal__UsandoTipoDeInstancia', () => {

    test('si es del tipo Animal retorna true', () => {
        const toto: Animal = Animal.DeNombre(TOTO_NOMBRE);

        assert.that(EsUnAnimal__UsandoTipoDeInstancia(toto)).isEqualTo(true);
    });

    test('si no es del tipo Animal retorna false', () => {
        const fakeToco: unknown = {};
        assert.that(EsUnAnimal__UsandoTipoDeInstancia(fakeToco)).isEqualTo(false);
    });

});

suite('Mascota', () => {

    test('una mascota tiene dueño y nombre', () => {
        const dorothy: Persona = Persona.DeNombre(DOROTHY_NOMBRE, DOROTHY_PROFESION);
        const toto: Mascota = Mascota.De(dorothy, TOTO_NOMBRE);
        assert.that(toto.seLlama(TOTO_NOMBRE)).isEqualTo(true);
        assert.that(toto.esDuenio(dorothy)).isEqualTo(true);
    });

    test('el nombre no puede ser blancos', () => {
        const dorothy: Persona = Persona.DeNombre(DOROTHY_NOMBRE, DOROTHY_PROFESION);
        assert.that(() => Mascota.De(dorothy, '')).raises(Error(Mascota.EL_NOMBRE_NO_PUEDE_ESTAR_EN_BLANCO));
    });

    test('se puede deserializar de un string JSON', () => {
        const jsonObject: unknown = {
            nombre: TOTO_NOMBRE,
            duenio: {
                nombre: DOROTHY_NOMBRE,
                profesion: DOROTHY_PROFESION
            }
        };

        const toto: Mascota = SerializationHelper.deserializarUsandoTypesDict(Mascota, jsonObject);
        const dorothy: Persona = Persona.DeNombre(DOROTHY_NOMBRE, DOROTHY_PROFESION);
        assert.that(toto.seLlama(TOTO_NOMBRE)).isEqualTo(true);
        assert.that(toto.esDuenio(dorothy)).isEqualTo(true);
    });


});