import { Animal, EsUnaInterfazDeAnimal__UsandoPropiedad, EsUnAnimal__UsandoPropiedad, EsUnAnimal__UsandoTipoDeInstancia } from "./mascota";
import { suite, test, assert } from '@pmoo/testy';

/**
 * Test para mostrar potenciales problemas con el uso de type guards
 * Referencia: https://www.typescriptlang.org/docs/handbook/advanced-types.html
 */
suite('Type Guards', () => {
    test('identificar el tipo solo por un campo no asegura que sea válido', () => {
        const posibleAnimal: unknown = { nombre: 'Toto' };
        if (EsUnaInterfazDeAnimal__UsandoPropiedad(posibleAnimal)) {
            //A partir de aqui posibleAnimal es del tipo InterfazDeAnimal
            //Es valido por la definicion de interfaz
            assert.that(posibleAnimal.nombre).isEqualTo('Toto');

            if (EsUnAnimal__UsandoPropiedad(posibleAnimal)) {
                //A partir de aqui possibleAnimal es instancia de Animal
                assert.that(posibleAnimal.nombre).isEqualTo('Toto');
                //Falla porque el objeto no tiene el método seLlama
                assert.that(() => posibleAnimal.seLlama('Toto')).raises(TypeError('posibleAnimal.seLlama is not a function'));
            }
        }

    });

    test('identificar el tipo solo por su clase no asegura que sea válido', () => {
        const possibleAnimal: unknown = {};
        //Si bien su uso es desaconsejado, se puede usar.
        Object.setPrototypeOf(possibleAnimal, Animal.prototype);
        if (EsUnAnimal__UsandoTipoDeInstancia(possibleAnimal)) {
            //A partir de aqui possibleAnimal es instancia de Animal
            //Puedo llamar a
            assert.that(possibleAnimal.seLlama('Toto')).isEqualTo(false);
            assert.that(possibleAnimal.nombre).isUndefined();
        }

    });

});
