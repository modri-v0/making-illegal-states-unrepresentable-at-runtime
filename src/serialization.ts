//Clases que tiene constructor
export type Clazz<T> = new(...args: any[]) => T;

/**
 * Interfaz para definir los tipo para utilizar con SerializationHelper
 */
export interface Deserializable<T> {

    /**
     * Metodo para obtener una instancia del tipo a partir de un objeto
     * @param object 
     */
    deserealize(object: any): T;

    /**
     * Propiedad para definir un mapa/diccionario 
     * de los tipos de las propiedades de la clase
     */
    types: { [index: string]: any };
}

/**
 * Funcion para evaluar si un objeto es instanci
 * @param anObject 
 */
function IsDeserializableType<T>(anObject: any): anObject is Deserializable<T> {
    return typeof anObject === "object" && anObject != null &&
       'deserealize' in anObject && typeof Reflect.get(anObject, 'deserealize') === 'function';
  }

export class SerializationHelper {

    /**
     * Metodo para crear una instancia del tipo T
     * a partir de un objeto generico, usualmente
     * un JSON convertido en un tipo básico de JavaScript
     * 
     * Visto en OpenAPI generator
     * @param type constructor de la clase
     * @param jsonValue valor u objeto Javascript deserealizado de un JSON
     */
    static deserializarUsandoTypesDict<T>(type: Clazz<T>, jsonValue: any) : T {

        const newInstance = new type();
        if(IsDeserializableType<T>(newInstance)) {
            for (const key in newInstance.types) {
                const rawValue = Reflect.get(jsonValue, key);
                const value = SerializationHelper.deserializarUsandoTypesDict(newInstance.types[key] , rawValue);
                Reflect.set(newInstance, key, value);
            }
            return newInstance;
        }

        return jsonValue;
    }

    /**
     * Deserealizacion usando la implementacion del metodo
     * de la interfaz
     * @param type constructor de la clase
     * @param jsonValue valor u objeto Javascript deserealizado de un JSON
     */
    static deserializarUsandoMetodoClase<T>(type: Clazz<T>, jsonObject: any) : T {

        const newInstance = new type();
        if(IsDeserializableType<T>(newInstance)) {
            return newInstance.deserealize(jsonObject);
        }

        return jsonObject;
    }

}