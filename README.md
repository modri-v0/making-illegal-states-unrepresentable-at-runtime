# Making illegal states unrepresentable at runtime (in Typescript)

Código que acompaña al post [Haciendo ilegales los estados no representables en tiempo de ejecución (en TypeScript)](https://modri-v0.gitlab.io/posts/haciendo-ilegales-los-estados-no-representables-en-tiempo-de-ejecucion-en-typescript/).

Las pruebas están hechas con [@pmoo/testy](https://www.npmjs.com/package/@pmoo/testy) para minimizar las dependencias. Para el detalle de como usar @pmoo/testy con `Typescript` ver [NOTES.md](NOTES.md)